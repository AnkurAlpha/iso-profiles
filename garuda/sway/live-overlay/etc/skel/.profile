# Set a few environment variables for the live session

export XDG_SESSION_TYPE=wayland
export XDG_SESSION_DESKTOP=sway
export XDG_CURRENT_DESKTOP=sway
export QT_QPA_PLATFORM=wayland
export QT_STYLE_OVERRIDE=kvantum
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
export EDITOR=/usr/bin/micro
export BROWSER=librewolf
export TERM=foot
export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1
